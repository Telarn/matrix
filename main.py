import csv
import os

from sqlalchemy import Column, Integer, String, create_engine, ForeignKey, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.orm.exc import NoResultFound

Base = declarative_base()

# fp = 'database.db'
# if os.path.exists(fp):
#     os.remove(fp)

file = 'DATA.csv'


class Dealer(Base):
    __tablename__ = 'dealer'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    employees = relationship(
        'Product',
        secondary='dealer_product_link'
    )


class Product(Base):
    __tablename__ = 'product'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    departments = relationship(
        Dealer,
        secondary='dealer_product_link'
    )


class Price(Base):
    __tablename__ = 'dealer_product_link'
    dealer_id = Column(Integer, ForeignKey('dealer.id'), primary_key=True)
    product_id = Column(Integer, ForeignKey('product.id'), primary_key=True)
    price = Column(Integer)
    dealer = relationship(Dealer, backref=backref("product_assoc"))
    product = relationship(Product, backref=backref("dealer_assoc"))


engine = create_engine('sqlite:///database.db')
session = sessionmaker()
session.configure(bind=engine)
Base.metadata.create_all(engine)
s = session()


def read(filename):
    a = []
    with open(filename, newline='') as f:
        reader = csv.reader(f)
        for row in sorted(reader):
            a.append(row[0].split(sep='\t'))
    return a


def get_one_or_create(sess, model, **kwargs):
    try:
        return sess.query(model).filter_by(**kwargs).one()
    except NoResultFound:
        return model(**kwargs)


def db_fill(file_content):
    for z in file_content:
        deal = get_one_or_create(s, Dealer, name=z[0])
        prod = get_one_or_create(s, Product, name=z[1])
        comm = Price(dealer=deal, product=prod, price=z[2])
        s.add(comm)
        s.commit()


def prod_list_by_name(deal_name):
    query = s.query(Price).join(Dealer).filter(Dealer.name == deal_name).all()
    prod_list = [p.product.name for p in query]
    return prod_list


def intersection(list1, list2):
    inter_list = []
    for l in list1:
        if l in list2:
            inter_list.append(l)
    return inter_list


def get_price(prod, deal):
    return s.query(Price).join(Dealer, Product).filter(Product.name == prod, Dealer.name == deal).order_by(
        Price.price).first().price


# db_fill(read(file))
counter = 1
dealer_list = [x.name for x in s.query(Dealer).all()]
matrix1 = [[0 for x in range(len(dealer_list))] for y in range(len(dealer_list))]
matrix2 = [[0 for x in range(len(dealer_list))] for y in range(len(dealer_list))]
for x in range(counter, len(dealer_list) + 1):
    for z in range(counter + 1, len(dealer_list) + 1):
        inter = intersection(prod_list_by_name(dealer_list[x - 1]), prod_list_by_name(dealer_list[z - 1]))
        matrix1[x - 1][z - 1] = len(inter)
        matrix1[z - 1][x - 1] = len(inter)
        for i in inter:
            if get_price(i, dealer_list[x - 1]) == get_price(i, dealer_list[z - 1]):
                pass
            elif get_price(i, dealer_list[x - 1]) < get_price(i, dealer_list[z - 1]):
                matrix2[x - 1][z - 1] += 1
            else:
                matrix2[z - 1][x - 1] += 1
    matrix1[x-1].insert(0, dealer_list[x-1])
    matrix2[x-1].insert(0, dealer_list[x-1])
    counter += 1
matrix1.insert(0, dealer_list)
matrix1[0].insert(0, ' ')
matrix2.insert(0, dealer_list)
with open('matrix1.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(matrix1)
with open('matrix2.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(matrix2)

